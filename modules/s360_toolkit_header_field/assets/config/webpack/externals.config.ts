// Externals
// @see https://webpack.js.org/configuration/externals/

let externals = {
  'jquery': 'jQuery'
};

export { externals as WEBPACK_EXTERNALS };