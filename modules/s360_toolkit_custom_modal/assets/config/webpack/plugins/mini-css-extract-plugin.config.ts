// MiniCssExtractPlugin
// @see https://webpack.js.org/plugins/mini-css-extract-plugin/#options

let MiniCssExtractPluginConfig = {
  filename: `css/[name].min.css`,
};

export default MiniCssExtractPluginConfig;