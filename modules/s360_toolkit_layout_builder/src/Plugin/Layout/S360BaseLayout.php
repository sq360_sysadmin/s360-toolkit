<?php

namespace Drupal\s360_toolkit_layout_builder\Plugin\Layout;

use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Base layout plugin class.
 */
class S360BaseLayout extends LayoutDefault implements PluginFormInterface {

}
