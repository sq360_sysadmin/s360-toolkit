export { WEBPACK_EXTERNALS } from './externals.config';

export { WEBPACK_STATS } from './stats.config';

export { WEBPACK_PLUGINS } from './plugins';

export { WEBPACK_OPTIMIZATION_MINIMIZER } from './optimization';

export {
  WEBPACK_RESOLVE_ALIAS,
  WEBPACK_RESOLVE_EXTENSIONS
} from './resolve';

export { WEBPACK_MODULE_RULES } from './module';